<?php

namespace app;

require 'bootstrap.php';

use Carbon\Carbon;

class validate_jwt {


function CreateJwt($env){

    $username = $env["USER"];
    $password = $env["PASSWORD"];
    $secret = $env["SECRET"];

    $header = json_encode([
        'typ' => 'JWT',
        'alg' => 'HS256'
    ]);

    $paylod = json_encode([
        'user_id' => 1,
        'role' => 'USER',
        'username' => $username,
        'password' => $password,
        'exp' => 10000000000000
    ]);

    $header64 = base64UrlEncode($header);
    $paylod64 = base64UrlEncode($paylod);

    $signature = hash_hmac('sha256', $header64 . "." . $paylod64, $secret, true);

    $signature64 = base64UrlEncode($signature);

    $jwt = $header64 . "." . $paylod64 . "." . $signature64;

    $tokenParts = explode('.', $jwt);
    $header =  base64_decode($tokenParts[0]);
    $paylod = base64_decode($tokenParts[1]);
    $signatureProvided = $tokenParts[2];

    $expiration = Carbon::createFromTimestamp(json_decode($paylod)->exp);
    $tokenExpired = (Carbon::now()->diffInMinutes($expiration, false) < 0);

    $header64 = base64UrlEncode($header);
    $paylod64 = base64UrlEncode($paylod);
    $signature = hash_hmac('sha256', $header64 . "." . $paylod64, $secret, true);
    $signature64 = base64UrlEncode($signature);

    if ( $tokenExpired ){
        echo "TOKEN EXPIRED!!! \n";
    } else {
        echo "TOKEN IS NOT EXPIRED!!! \n";
    }

    if( $signatureProvided === $signature64 ){
        echo "SIGNATURE VALID!!! \n";
    } else {
        echo "SIGNATURE NOT VALID!!! \n";
    }

    return $jwt;

    }
}

