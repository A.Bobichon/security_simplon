<?php
require 'vendor/autoload.php';

$dotenv =  Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

function base64UrlEncode($text)
{
    return str_replace(
        ['+', '/', '='],
        ['-', '_', ''],
        base64_encode($text)
    );
}